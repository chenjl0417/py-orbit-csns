#include "Multi_Harmonic_Cav.hh"
#include "ParticleMacroSize.hh"

#include <iostream>
#include <cmath>

#include "Bunch.hh"
#include "OrbitConst.hh"

using namespace OrbitUtils;

// Constructor
Multi_Harmonic_Cav::Multi_Harmonic_Cav(double ZtoPhi, double dESync,
                           double *RFHarms, double *RFVoltages,
                           double *RFPhases, int nRFHarmonics): CppPyWrapper(NULL)
{
  _ZtoPhi     = ZtoPhi;
  _dESync     = dESync;
  _nRFHarmonics = nRFHarmonics;
  _RFHarms = new double[nRFHarmonics];
  _RFVoltages = new double[nRFHarmonics];
  _RFPhases = new double[nRFHarmonics];
  for(int i=0; i < nRFHarmonics; i++)
  {
    _RFHarms[i]    = RFHarms[i];
    _RFVoltages[i] = RFVoltages[i];
    _RFPhases[i]   = RFPhases[i];
  }
}

// Destructor
Multi_Harmonic_Cav::~Multi_Harmonic_Cav()
{
    delete [] _RFHarms;
    delete [] _RFVoltages;
    delete [] _RFPhases;
}

void Multi_Harmonic_Cav::setZtoPhi(double ZtoPhi)
{
  _ZtoPhi = ZtoPhi;
}

double Multi_Harmonic_Cav::getZtoPhi()
{
  return _ZtoPhi;
}

void Multi_Harmonic_Cav::setdESync(double dESync)
{
  _dESync = dESync;
}

double Multi_Harmonic_Cav::getdESync()
{
  return _dESync;
}

int Multi_Harmonic_Cav::getnRFHarmonics()
{
    return _nRFHarmonics;
}

void Multi_Harmonic_Cav::setRFVoltages(double RFVoltages[])
{
    for(int i=0; i< _nRFHarmonics; i++)
    {
        _RFVoltages[0] = RFVoltages[0];
    }
}

double* Multi_Harmonic_Cav::getRFVoltages()
{
  return _RFVoltages;
}

void Multi_Harmonic_Cav::setRFPhases(double RFPhases[])
{
    for(int i=0; i< _nRFHarmonics; i++)
    {
        _RFPhases[0] = RFPhases[0];
    }
}

double* Multi_Harmonic_Cav::getRFPhases()
{
  return _RFPhases;
}

void Multi_Harmonic_Cav::setOldGamma(double Gamma)
{
    _OldGamma = Gamma;
}

double Multi_Harmonic_Cav::getOldGamma()
{
    return _OldGamma;
}

void Multi_Harmonic_Cav::setOldBeta(double Beta)
{
    _OldBeta = Beta;
}

double Multi_Harmonic_Cav::getOldBeta()
{
    return _OldBeta;
}

void Multi_Harmonic_Cav::trackBunch(Bunch* bunch)
{
  double ZtoPhi    = _ZtoPhi;
  double dESync    = _dESync;
  double OldGamma  = _OldGamma;
  double OldBeta   = _OldBeta;
  double nRFHarmonics = _nRFHarmonics;

  double phase, RFPhase, dESum;

  bunch->compress();
  SyncPart* syncPart = bunch->getSyncPart();
  double** arr = bunch->coordArr();
  double Gamma = syncPart->getGamma();
  double Beta = syncPart->getBeta();

  for(int i = 0; i < bunch->getSize(); i++)
  {
      dESum = 0.0;
      phase = -ZtoPhi * arr[i][4];
      for (int j = 0; j < nRFHarmonics; j++)
      {
        RFPhase = OrbitConst::PI*(_RFPhases[j]) / 180.0;
        dESum += bunch->getCharge()* _RFVoltages[j] * sin(_RFHarms[j] * phase + RFPhase);
      }
    arr[i][5] += dESum - dESync;
    arr[i][1] *= OldGamma*OldBeta/(Gamma*Beta);
    arr[i][3] *= OldGamma*OldBeta/(Gamma*Beta);
  }
}
